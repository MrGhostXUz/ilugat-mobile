import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:ilugat/main.dart';
import 'package:ilugat/vertical_tab.dart';
import 'package:ilugat/word.dart';
import 'package:share/share.dart';
import 'detail.dart';
import 'package:provider/provider.dart';

class MyHomePage extends StatefulWidget {
  static int screen = 1;
  static var baseHeight;
  static var baseWidth;
  static Word currentWord;

  MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  var _scaffold = GlobalKey<ScaffoldState>();
  bool isSearching;
  var searchItems;
  TextEditingController searchController;
  var _currentLetter;
  var data;
  var currentData;
  var currentTab = 0;
  var currentTabLt = 0;
  List<Word> favouriteWords = [];
  List<Word> historyWords = [];

  @override
  void initState() {
    super.initState();
    searchController = TextEditingController();
    isSearching = false;
    if (Word.appAlphabet == 0) {
      _currentLetter = Word.wordsLt.keys.toList()[currentTabLt];
    } else {
      _currentLetter = Word.words.keys.toList()[currentTab];
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    favouriteWords.clear();
    if (Word.favourites.isNotEmpty) {
      for (int id in Word.favourites) {
        Word word = Word.fromJson(
            Word.resource.singleWhere((element) => element['id'] == id));
        word.isFavourite = true;
        favouriteWords.add(word);
      }
    }
    historyWords.clear();
    if (Word.history.isNotEmpty) {
      for (int id in Word.history) {
        Word word = Word.fromJson(
            Word.resource.singleWhere((element) => element['id'] == id));
        historyWords.add(word);
      }
    }
    if (Word.appAlphabet == 0) {
      data = [Word.wordsLt[_currentLetter], favouriteWords, historyWords];
    } else {
      data = [Word.words[_currentLetter], favouriteWords, historyWords];
    }
    currentData = data[MyHomePage.screen - 1];

    var screenItems = ListView.builder(
        padding: EdgeInsets.only(top: 5.0),
        itemCount: currentData.length,
        itemBuilder: (context, index) {
          return Column(
            children: <Widget>[
              FlatButton(
                onPressed: () {
                  setState(() {
                    MyHomePage.currentWord = currentData[index];

                    Word.addToHistory(MyHomePage.currentWord);
                  });
                },
                child: Container(
                  height: 50.0,
                  alignment: Alignment.centerLeft,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Expanded(
                        flex: 28,
                        child: Text(
                          (Word.appAlphabet == 1)
                              ? currentData[index].word
                              : currentData[index].wordLt,
                          style: GoogleFonts.ptSans(
                              color: Color(0xFF030a4e),
                              fontWeight: FontWeight.normal),
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Icon(
                          Icons.chevron_right,
                          color: Color(0xFF030a4e),
                          size: 15.0,
                        ),
                      )
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 0.5,
                child: Container(
                  color: Color(0xFF030a4e).withOpacity(0.2),
                ),
              ),
            ],
          );
        });

    VerticalTabs tabsForCyrill = VerticalTabs(
      initialIndex: currentTab,
      onSelect: (number) {
        currentTab = number;
        if (WidgetsBinding.instance.lifecycleState ==
            AppLifecycleState.resumed) {
          setState(() {
            MyHomePage.screen = 1;
            _currentLetter = Word.words.keys.toList()[number];
          });
        }
      },
      tabsWidth: MediaQuery.of(context).size.width * 1 / 5,
      backgroundColor: Colors.white,
      tabs: Word.words.keys
          .toList()
          .map(
            (e) => Tab(
              text: e,
            ),
          )
          .toList(),
      selectedTabTextStyle: GoogleFonts.ptSans(
        color: Color(0xFF35c79c),
        shadows: [
          BoxShadow(blurRadius: 2, color: Color(0xFFa4fff7)),
        ],
        fontSize: 17.0,
        fontWeight: FontWeight.w600,
      ),
      tabTextStyle: GoogleFonts.ptSans(
        color: Color(0xFF030a4e),
        shadows: [
          BoxShadow(
            blurRadius: 2,
            color: Color(0x55030a4e),
          ),
        ],
        fontSize: 17.0,
        fontWeight: FontWeight.w600,
      ),
    );
    VerticalTabs tabsForLatin = VerticalTabs(
      initialIndex: currentTabLt,
      onSelect: (number) {
        currentTabLt = number;
        if (WidgetsBinding.instance.lifecycleState ==
            AppLifecycleState.resumed) {
          setState(() {
            MyHomePage.screen = 1;
            _currentLetter = Word.wordsLt.keys.toList()[number];
          });
        }
      },
      tabsWidth: MediaQuery.of(context).size.width * 1 / 5,
      backgroundColor: Colors.white,
      tabs: Word.wordsLt.keys
          .toList()
          .map(
            (e) => Tab(
              text: e,
            ),
          )
          .toList(),
      selectedTabTextStyle: GoogleFonts.ptSans(
        color: Color(0xFF35c79c),
        shadows: [
          BoxShadow(blurRadius: 2, color: Color(0xFFa4fff7)),
        ],
        fontSize: 17.0,
        fontWeight: FontWeight.w600,
      ),
      tabTextStyle: GoogleFonts.ptSans(
        color: Color(0xFF030a4e),
        shadows: [
          BoxShadow(
            blurRadius: 2,
            color: Color(0x55030a4e),
          ),
        ],
        fontSize: 17.0,
        fontWeight: FontWeight.w600,
      ),
    );

    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
        if (searchController.text == '') {
          setState(() {
            isSearching = false;
          });
        }
      },
      child: ChangeNotifierProvider<AlphabetChangingNotifier>(
        create: (context) => AlphabetChangingNotifier(),
        child: Scaffold(
          key: _scaffold,
          backgroundColor: Color(0xEEFFFFFF),
          endDrawer: Drawer(
            child: SafeArea(
              child: Container(
                color: Colors.white,
                width: MediaQuery.of(context).size.height,
                child: Column(
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: Column(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.symmetric(
                              vertical:
                                  MediaQuery.of(context).size.height * 0.1 / 10,
                              horizontal:
                                  MediaQuery.of(context).size.height * 1 / 20,
                            ),
                            child: Container(
                              height: MediaQuery.of(context).size.height * 0.18,
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Expanded(
                                    flex: 2,
                                    child: Image(
                                      image: AssetImage('small_logo.png'),
                                    ),
                                  ),
                                  Expanded(
                                    flex: 1,
                                    child: Container(),
                                  ),
                                  Expanded(
                                    flex: 9,
                                    child: Center(
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Text(
                                            Word.appLabels['title_home_part_1']
                                                    [Word.appAlphabet]
                                                .toUpperCase(),
                                            style: GoogleFonts.ubuntu(
                                              color: Color(0xFF030a4e),
                                              fontWeight: FontWeight.w900,
                                              letterSpacing: 1,
                                              fontSize: 12,
                                            ),
                                          ),
                                          Text(
                                            Word.appLabels['title_home_part_2']
                                                    [Word.appAlphabet]
                                                .toUpperCase(),
                                            style: GoogleFonts.ubuntu(
                                              color: Color(0xFF32c89b),
                                              fontWeight: FontWeight.w900,
                                              letterSpacing: 1,
                                              fontSize: 17,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 0.2,
                      child: Container(
                        color: Color(0xFF32c89b),
                      ),
                    ),
                    Expanded(
                      flex: 3,
                      child: Container(
                        child: Column(
                          children: <Widget>[
                            Container(
                              decoration: BoxDecoration(
                                color: (MyHomePage.screen == 1)
                                    ? Color(0xFF32c89b)
                                    : Colors.white,
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(20.0),
                                  bottomLeft: Radius.circular(20.0),
                                ),
                              ),
                              child: FlatButton(
                                onPressed: () {
                                  setState(() {
                                    searchController.clear();
                                    isSearching = false;
                                    MyHomePage.screen = 1;
                                  });
                                  Navigator.of(context).pop();
                                },
                                child: Container(
                                  height: 50.0,
                                  padding: EdgeInsets.only(
                                      left: 20.0,
                                      right: 5.0,
                                      bottom: 12.0,
                                      top: 12.0),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Expanded(
                                        flex: 5,
                                        child: Text(
                                          Word.appLabels['title_home_drawer']
                                              [Word.appAlphabet],
                                          style: GoogleFonts.ptSans(
                                            color: (MyHomePage.screen == 1)
                                                ? Colors.white
                                                : Color(0xFF32c89b),
                                            fontSize: 16.0,
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 1,
                                        child: Icon(
                                          Icons.home,
                                          color: (MyHomePage.screen == 1)
                                              ? Colors.white
                                              : Color(0xFF32c89b),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            Container(
                              decoration: BoxDecoration(
                                color: (MyHomePage.screen == 2)
                                    ? Color(0xFF32c89b)
                                    : Colors.white,
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(20.0),
                                  bottomLeft: Radius.circular(20.0),
                                ),
                              ),
                              child: FlatButton(
                                onPressed: () {
                                  setState(() {
                                    searchController.clear();
                                    isSearching = false;
                                    MyHomePage.screen = 2;
                                  });
                                  Navigator.of(context).pop();
                                },
                                child: Container(
                                  padding: EdgeInsets.only(
                                      left: 20.0,
                                      right: 5.0,
                                      bottom: 12.0,
                                      top: 12.0),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Expanded(
                                        flex: 5,
                                        child: Text(
                                          Word.appLabels['title_favourites']
                                              [Word.appAlphabet],
                                          style: GoogleFonts.ptSans(
                                            color: (MyHomePage.screen == 2)
                                                ? Colors.white
                                                : Color(0xFF32c89b),
                                            fontSize: 16.0,
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 1,
                                        child: Icon(
                                          Icons.favorite,
                                          color: (MyHomePage.screen == 2)
                                              ? Colors.white
                                              : Color(0xFF32c89b),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            Container(
                              decoration: BoxDecoration(
                                color: (MyHomePage.screen == 3)
                                    ? Color(0xFF32c89b)
                                    : Colors.white,
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(20.0),
                                  bottomLeft: Radius.circular(20.0),
                                ),
                              ),
                              child: FlatButton(
                                onPressed: () {
                                  setState(() {
                                    searchController.clear();
                                    isSearching = false;
                                    MyHomePage.screen = 3;
                                  });
                                  Navigator.of(context).pop();
                                },
                                child: Container(
                                  padding: EdgeInsets.only(
                                      left: 20.0,
                                      right: 5.0,
                                      bottom: 12.0,
                                      top: 12.0),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Expanded(
                                        flex: 5,
                                        child: Text(
                                          Word.appLabels['title_history']
                                              [Word.appAlphabet],
                                          style: GoogleFonts.ptSans(
                                            color: (MyHomePage.screen == 3)
                                                ? Colors.white
                                                : Color(0xFF32c89b),
                                            fontSize: 16.0,
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 1,
                                        child: Icon(
                                          Icons.history,
                                          color: (MyHomePage.screen == 3)
                                              ? Colors.white
                                              : Color(0xFF32c89b),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 0.2,
                              child: Container(
                                color: Color(0xFF32c89b),
                              ),
                            ),
                            Container(
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(20.0),
                                  bottomLeft: Radius.circular(20.0),
                                ),
                              ),
                              child: Consumer<AlphabetChangingNotifier>(
                                builder: (context, provider, child) =>
                                    FlatButton(
                                  onPressed: () {
                                    setState(() {
                                      searchController.clear();
                                      isSearching = false;
                                      Word.appAlphabet = 1 - Word.appAlphabet;
                                      MyApp.wordBase
                                          .put('appAlphabet', Word.appAlphabet);
                                      if (Word.appAlphabet == 0) {
                                        _currentLetter = Word.wordsLt.keys
                                            .toList()[currentTabLt];
                                      } else {
                                        _currentLetter = Word.words.keys
                                            .toList()[currentTab];
                                      }
                                      provider.mainScreenUpdate();
                                    });
                                    Navigator.of(context).pop();
                                  },
                                  child: Container(
                                    padding: EdgeInsets.symmetric(
                                        vertical: 12.0,
                                        horizontal:
                                            MediaQuery.of(context).size.width *
                                                0.15),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Text(
                                          (Word.appAlphabet == 0)
                                              ? 'Lotin'
                                              : 'Кирилл',
                                          style: GoogleFonts.ptSans(
                                            color: Color(0xFF32c89b),
                                            fontSize: 16.0,
                                          ),
                                        ),
                                        Icon(
                                          Icons.find_replace,
                                          color: Colors.brown,
                                        ),
                                        Text(
                                          (Word.appAlphabet == 0)
                                              ? 'Кирилл'
                                              : 'Lotin',
                                          style: GoogleFonts.ptSans(
                                            color: Color(0xFF030a4e),
                                            fontSize: 16.0,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 0.2,
                              child: Container(
                                color: Color(0xFF32c89b),
                              ),
                            ),
                            Container(
                              color: Colors.white,
                              child: FlatButton(
                                onPressed: () {
                                  Share.share(
                                    'https://play.google.com/store/apps/details?id=com.masterx.ilugat',
                                    subject: Word.appLabels['title_home_part_1']
                                                [Word.appAlphabet]
                                            .toUpperCase() +
                                        ' ' +
                                        Word.appLabels['title_home_part_2']
                                                [Word.appAlphabet]
                                            .toUpperCase(),
                                  );
                                },
                                child: Container(
                                  height: 50.0,
                                  padding: EdgeInsets.only(
                                      left: 20.0,
                                      right: 5.0,
                                      bottom: 12.0,
                                      top: 12.0),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Expanded(
                                        flex: 5,
                                        child: Text(
                                          Word.appLabels['title_share']
                                              [Word.appAlphabet],
                                          style: GoogleFonts.ptSans(
                                            color: Color(0xFF32c89b),
                                            fontSize: 16.0,
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 1,
                                        child: Icon(
                                          Icons.share,
                                          color: Color(0xFF32c89b),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          body: SingleChildScrollView(
            child: Container(
              color: Color(0xFFf1f6fc),
              height: MediaQuery.of(context).size.height,
              child: Row(
                children: [
                  FadeInLeft(
                    duration: Duration(milliseconds: 500),
                    child: Container(
                      color: Colors.white,
                      child: Column(
                        children: [
                          Container(
                            width: MediaQuery.of(context).size.width * 1 / 5,
                            color: Colors.white,
                            height:
                                MediaQuery.of(context).size.height * 0.3 - 0.5,
                            child: FlatButton(
                              padding: EdgeInsets.zero,
                              onPressed: () {
                                _scaffold.currentState.openEndDrawer();
                              },
                              child: Padding(
                                padding: EdgeInsets.symmetric(
                                    vertical:
                                        MediaQuery.of(context).size.height *
                                            0.1),
                                child: Center(
                                  child: Container(
                                    height: MediaQuery.of(context).size.height *
                                        0.1,
                                    child: Image(
                                      width: 25.0,
                                      image: AssetImage('fourDot.png'),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Container(
                            height: 0.5,
                            width: MediaQuery.of(context).size.width * 1 / 6,
                            color: Colors.grey.withOpacity(0.5),
                          ),
                          SingleChildScrollView(
                            scrollDirection: Axis.vertical,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Container(
                                  height:
                                      MediaQuery.of(context).size.height * 0.7,
                                  child: (Word.appAlphabet == 1)
                                      ? tabsForCyrill
                                      : tabsForLatin,
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SingleChildScrollView(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        SizedBox(
                          height: MediaQuery.of(context).size.height * 0.02,
                        ),
                        FadeInDown(
                          duration: Duration(milliseconds: 500),
                          child: Container(
                            height: MediaQuery.of(context).size.height * 0.135,
                            width: MediaQuery.of(context).size.width * 0.6,
                            child: Row(
                              children: [
                                (MyHomePage.screen == 1)
                                    ? Icon(
                                        Icons.home,
                                        color: Color(0xFF35c79c),
                                      )
                                    : (MyHomePage.screen == 2)
                                        ? Icon(
                                            Icons.favorite,
                                            color: Color(0xFF35c79c),
                                          )
                                        : Icon(
                                            Icons.history,
                                            color: Color(0xFF35c79c),
                                          ),
                              ],
                            ),
                          ),
                        ),
                        (MyHomePage.currentWord != null)
                            ? Container(
                                width: MediaQuery.of(context).size.width * 0.76,
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    FadeInLeft(
                                      duration: Duration(milliseconds: 500),
                                      child: IconButton(
                                        icon: Icon(
                                          Icons.arrow_back_ios,
                                          color: Color(0xFF030a4e),
                                        ),
                                        onPressed: () {
                                          setState(() {
                                            MyHomePage.currentWord = null;
                                          });
                                        },
                                      ),
                                    ),
                                    FadeInRight(
                                      duration: Duration(milliseconds: 500),
                                      child: IconButton(
                                        icon: Icon(
                                          (Word.favourites.contains(
                                                  MyHomePage.currentWord.id))
                                              ? Icons.favorite
                                              : Icons.favorite_border,
                                          color: Color(0xFF32c89b),
                                        ),
                                        onPressed: () {
                                          setState(() {
                                            Word.changeFavouriteParam(
                                                MyHomePage.currentWord);
                                          });
                                        },
                                      ),
                                    )
                                  ],
                                ),
                              )
                            : Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal:
                                        MediaQuery.of(context).size.width / 10),
                                child: FadeInDown(
                                  duration: Duration(milliseconds: 500),
                                  child: Container(
                                    width:
                                        MediaQuery.of(context).size.width * 0.6,
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          Word.appLabels['title_home_part_1']
                                                  [Word.appAlphabet]
                                              .toUpperCase(),
                                          style: GoogleFonts.ubuntu(
                                            color: Color(0xFF030a4e),
                                            fontWeight: FontWeight.w900,
                                            letterSpacing: 1,
                                            fontSize: 16,
                                          ),
                                        ),
                                        Text(
                                          Word.appLabels['title_home_part_2']
                                                  [Word.appAlphabet]
                                              .toUpperCase(),
                                          style: GoogleFonts.ubuntu(
                                            color: Color(0xFF32c89b),
                                            fontWeight: FontWeight.w900,
                                            letterSpacing: 1,
                                            fontSize: 25,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                        (MyHomePage.currentWord != null)
                            ? FadeInRight(
                                duration: Duration(milliseconds: 500),
                                child: Detail(word: MyHomePage.currentWord))
                            : Container(),
                        (MyHomePage.currentWord != null)
                            ? SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.02,
                              )
                            : SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.05,
                              ),
                        FadeInRight(
                          duration: Duration(milliseconds: 500),
                          child: Container(
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(32.0),
                            ),
                            width: MediaQuery.of(context).size.width * 0.7,
                            child: TextField(
                              controller: searchController,
                              onChanged: (String query) {
                                setState(
                                  () {
                                    var founded = [];
                                    isSearching = true;
                                    if (Word.appAlphabet == 1) {
                                      founded = currentData
                                          .where((item) => item.word
                                              .toString()
                                              .contains(query.toUpperCase()))
                                          .toList();
                                    } else {
                                      founded = currentData
                                          .where((item) => item.wordLt
                                              .toString()
                                              .contains(query.toUpperCase()))
                                          .toList();
                                    }
                                    searchItems = ListView.builder(
                                        padding: EdgeInsets.only(top: 5.0),
                                        itemCount: founded.length,
                                        itemBuilder: (context, index) {
                                          return Column(
                                            children: <Widget>[
                                              FlatButton(
                                                onPressed: () {
                                                  setState(() {
                                                    MyHomePage.currentWord =
                                                        founded[index];
                                                    Word.addToHistory(
                                                        MyHomePage.currentWord);
                                                  });
                                                },
                                                child: Container(
                                                  height: 50.0,
                                                  alignment:
                                                      Alignment.centerLeft,
                                                  child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: <Widget>[
                                                      Expanded(
                                                        flex: 28,
                                                        child: Text(
                                                          (Word.appAlphabet ==
                                                                  1)
                                                              ? founded[index]
                                                                  .word
                                                              : founded[index]
                                                                  .wordLt,
                                                          style: GoogleFonts.ptSans(
                                                              color: Color(
                                                                  0xFF030a4e),
                                                              fontWeight:
                                                                  FontWeight
                                                                      .normal),
                                                          overflow: TextOverflow
                                                              .ellipsis,
                                                        ),
                                                      ),
                                                      Expanded(
                                                        flex: 1,
                                                        child: Icon(
                                                          Icons.chevron_right,
                                                          color:
                                                              Color(0xFF030a4e),
                                                          size: 15.0,
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                              ),
                                              SizedBox(
                                                height: 0.5,
                                                child: Container(
                                                  color: Color(0xFF030a4e)
                                                      .withOpacity(0.2),
                                                ),
                                              ),
                                            ],
                                          );
                                        });
                                  },
                                );
                              },
                              style:
                                  GoogleFonts.ptSans(color: Color(0xFF030a4e)),
                              keyboardType: TextInputType.text,
                              cursorColor: Color(0xFF030a4e),
                              decoration: InputDecoration(
                                hintStyle: TextStyle(color: Color(0xFF030a4e)),
                                hintText: Word.appLabels['title_search']
                                    [Word.appAlphabet],
                                contentPadding: EdgeInsets.only(
                                    top: 10.0, bottom: 10.0, left: 30.0),
                                enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.white, width: 1.0),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(32.0)),
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.white, width: 2.0),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(32.0)),
                                ),
                                prefixIcon: IconButton(
                                  onPressed: () {
                                    if (isSearching) {
                                      setState(() {
                                        searchController.text = '';
                                        isSearching = false;
                                      });
                                    } else {}
                                  },
                                  icon: Padding(
                                    padding: const EdgeInsets.only(left: 10.0),
                                    child: Icon(
                                      Icons.search,
                                      color: Color(0xFF030a4e),
                                    ),
                                  ),
                                ),
                                suffixIcon: IconButton(
                                  onPressed: () {
                                    if (isSearching) {
                                      setState(() {
                                        searchController.text = '';
                                        isSearching = false;
                                      });
                                    } else {}
                                  },
                                  icon: Padding(
                                    padding: const EdgeInsets.only(right: 10.0),
                                    child: Icon(
                                      (isSearching) ? Icons.close : Icons.tune,
                                      color: Color(0xFF030a4e),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        FadeInRight(
                          duration: Duration(milliseconds: 500),
                          child: Container(
                            height:
                                MediaQuery.of(context).size.height * 2.1 / 3,
                            width: MediaQuery.of(context).size.width * 0.72,
                            child: (isSearching) ? searchItems : screenItems,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class AlphabetChangingNotifier extends ChangeNotifier {
  void mainScreenUpdate() {
    notifyListeners();
  }
}
