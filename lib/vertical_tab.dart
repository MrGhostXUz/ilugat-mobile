
import 'package:flutter/material.dart';

enum IndicatorSide { start, end }

/// A vertical tab widget for flutter
class VerticalTabs extends StatefulWidget {
  final Key key;
  final int initialIndex;
  final double tabsWidth;
  final double indicatorWidth;
  final IndicatorSide indicatorSide;
  final List<Tab> tabs;
  final TextDirection direction;
  final Color indicatorColor;
  final Color selectedTabBackgroundColor;
  final Color tabBackgroundColor;
  final TextStyle selectedTabTextStyle;
  final TextStyle tabTextStyle;
  final Color tabsShadowColor;
  final double tabsElevation;
  final Function(int tabIndex) onSelect;
  final Color backgroundColor;

  VerticalTabs(
      {this.key,
      @required this.tabs,
      this.tabsWidth,
      this.indicatorWidth = 3,
      this.indicatorSide,
      this.initialIndex = 0,
      this.direction = TextDirection.ltr,
      this.indicatorColor = const Color(0xFF35c79c),
      this.selectedTabBackgroundColor = const Color(0x2235c79c),
      this.tabBackgroundColor = Colors.white,
      this.selectedTabTextStyle,
      this.tabTextStyle,
      this.tabsShadowColor = Colors.black54,
      this.tabsElevation = 2.0,
      this.onSelect,
      this.backgroundColor})
      : assert(tabs != null),
        super(key: key);

  @override
  _VerticalTabsState createState() => _VerticalTabsState();
}

class _VerticalTabsState extends State<VerticalTabs>
    with TickerProviderStateMixin {
  int _selectedIndex;

  List<AnimationController> animationControllers = [];


  @override
  void initState() {
    _selectedIndex = widget.initialIndex;
    for (int i = 0; i < 34; i++) {
      animationControllers.add(AnimationController(
        vsync: this,
        duration: const Duration(milliseconds: 400),
      ));
    }
    _selectTab(widget.initialIndex);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: widget.direction,
      child: Container(
        color: Colors.white,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Expanded(
              child: Material(
                child: Container(
                  color: Colors.white,
                  width: widget.tabsWidth,
                  child: ListView.builder(
                    padding: EdgeInsets.only(bottom: 50.0),
                    itemCount: widget.tabs.length,
                    itemBuilder: (context, index) {
                      Tab tab = widget.tabs[index];

                      Alignment alignment = Alignment.center;
                      if (widget.direction == TextDirection.rtl) {
                        alignment = Alignment.centerRight;
                      }

                      Widget child;
                      if (tab.child != null) {
                        child = tab.child;
                      } else {
                        child = Container(
                            padding: EdgeInsets.all(10),
                            child: (tab.text != null)
                                ? Container(
                                    height: 40.0,
                                    width: widget.tabsWidth - 50,
                                    child: Center(
                                      child: Text(
                                        tab.text,
                                        softWrap: true,
                                        style: _selectedIndex == index
                                            ? widget.selectedTabTextStyle
                                            : widget.tabTextStyle,
                                      ),
                                    ))
                                : Container());
                      }

                      Color itemBGColor = widget.tabBackgroundColor;
                      if (_selectedIndex == index)
                        itemBGColor = widget.selectedTabBackgroundColor;

                      double left, right;
                      if (widget.direction == TextDirection.rtl) {
                        left = (widget.indicatorSide == IndicatorSide.end)
                            ? 0
                            : null;
                        right = (widget.indicatorSide == IndicatorSide.start)
                            ? 0
                            : null;
                      } else {
                        left = (widget.indicatorSide == IndicatorSide.start)
                            ? 0
                            : null;
                        right = (widget.indicatorSide == IndicatorSide.end)
                            ? 0
                            : null;
                      }

                      return Stack(
                        children: <Widget>[
                          Positioned(
                            top: 2,
                            bottom: 2,
                            width: widget.indicatorWidth,
                            left: left,
                            right: right,
                            child: ScaleTransition(
                              child: Container(
                                color: widget.indicatorColor,
                              ),
                              scale: Tween(begin: 0.0, end: 1.0).animate(
                                new CurvedAnimation(
                                  parent: animationControllers[index],
                                  curve: Curves.elasticOut,
                                ),
                              ),
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                _selectTab(index);
                              });
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                color: itemBGColor,
                              ),
                              alignment: alignment,
                              padding: EdgeInsets.all(5),
                              child: child,
                            ),
                          ),
                        ],
                      );
                    },
                  ),
                ),
                elevation: widget.tabsElevation,
                shadowColor: widget.tabsShadowColor,
                shape: BeveledRectangleBorder(),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> _selectTab(index) async {
    _selectedIndex = index;
    for (AnimationController animationController in animationControllers) {
      animationController.reset();
    }
    await animationControllers[index].forward();

    if (widget.onSelect != null) {
      widget.onSelect(_selectedIndex);
    }
  }
}
