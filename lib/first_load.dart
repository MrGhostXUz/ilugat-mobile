import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:ilugat/home.dart';
import 'package:ilugat/word.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'main.dart';
import 'word.dart';

class FirstLoad extends StatefulWidget {
  static int times = 1;

  @override
  _FirstLoadState createState() => _FirstLoadState();
}

class _FirstLoadState extends State<FirstLoad> {
  int process = 0;
  bool yesConnected = true;

  @override
  void initState() {
    super.initState();
    passToNext();
  }

  passToNext() async {
    Word.dio.options =
        BaseOptions(receiveTimeout: 50000, connectTimeout: 50000);
    try {
      await Word.needFullUpdate();
      await Word.getNewWordsNumber();
      if (MyApp.requestSize != 0) {
        for (int i = 0; i < FirstLoad.times; i++) {
          var response = Response();
          var failed = 0;
          while (response.statusCode != 200 && failed < 7) {
            try {
              response = await Word.getWordsFromDatabase(
                  i * MyApp.requestSize, (i + 1) * MyApp.requestSize);
            } catch (Exception) {
              await Future.delayed(Duration(milliseconds: 1));
              failed++;
            }
          }
          if (failed == 7) {
            break;
          }
          setState(() {
            process++;
          });
        }
      }
      await Word.getRemovedWordsFromDatabase(0, 1000);
      if (Word.resource.isEmpty) {
        Word.resource = await MyApp.resource.query('Words');
      }
      await MyApp.wordBase.clear();
      await MyApp.wordBase.put('favourites', Word.favourites);
      await MyApp.wordBase.put('history', Word.history);
      Word.lastUpdate = DateTime.now();
      await MyApp.wordBase.put('last_update', Word.lastUpdate);
      await MyApp.wordBase.put('fullUpdate', MyApp.baseFullUpdate);
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => MyHomePage()));
    } catch (Exception) {
      await Future.delayed(Duration(milliseconds: 500));
      if (Word.words['А'].isNotEmpty) {
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => MyHomePage()));
      } else {
        setState(() {
          yesConnected = false;
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.white,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.3,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 35.0),
              child: Container(
                height: MediaQuery.of(context).size.height * 0.3,
                child: Center(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        flex: 2,
                        child: Image(
                          image: AssetImage('small_logo.png'),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Container(),
                      ),
                      Expanded(
                        flex: 9,
                        child: Center(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                Word.appLabels['title_home_part_1']
                                        [Word.appAlphabet]
                                    .toUpperCase(),
                                style: TextStyle(
                                  color: Color(0xFF030a4e),
                                  fontWeight: FontWeight.w900,
                                  letterSpacing: 1,
                                  fontSize: 15,
                                ),
                              ),
                              Text(
                                Word.appLabels['title_home_part_2']
                                        [Word.appAlphabet]
                                    .toUpperCase(),
                                style: GoogleFonts.ubuntu(
                                  color: Color(0xFF35c79c),
                                  fontWeight: FontWeight.w900,
                                  letterSpacing: 1,
                                  fontSize: 24,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 15.0),
              height: MediaQuery.of(context).size.height * 0.3,
              child: (yesConnected)
                  ? Column(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Text(
                          Word.appLabels['first_load'][Word.appAlphabet],
                          maxLines: 4,
                          textAlign: TextAlign.justify,
                          style: GoogleFonts.ptSans(color: Colors.green),
                        ),
                        LinearPercentIndicator(
                          backgroundColor: Colors.greenAccent,
                          progressColor: Colors.green,
                          percent: process / FirstLoad.times,
                        ),
                        Center(
                          child: Padding(
                            padding: EdgeInsets.all(5.0),
                            child: Text(
                              (process * 100 ~/ FirstLoad.times).toString() +
                                  '%',
                              style: GoogleFonts.ptSans(color: Colors.green),
                            ),
                          ),
                        )
                      ],
                    )
                  : Container(
                      child: Text(
                        Word.appLabels['first_load_error'][Word.appAlphabet],
                        maxLines: 4,
                        textAlign: TextAlign.justify,
                        style: GoogleFonts.ptSans(color: Colors.green),
                      ),
                    ),
            ),
          ],
        ),
      ),
    );
  }
}
