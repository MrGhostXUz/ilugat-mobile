import 'package:dio/dio.dart';
import 'package:ilugat/first_load.dart';
import 'package:ilugat/main.dart';
import 'package:sqflite/sqflite.dart';


class Word {
  final int id;

  final String word;

  final String description;

  bool isFavourite = false;

  final String wordLt;

  final String descriptionLt;

  Word(this.id, this.word, this.description, this.wordLt, this.descriptionLt);

  factory Word.fromJson(dynamic json) {
    return Word(
      json['id'],
      json['word'].toString().trim().toUpperCase(),
      json['description'].toString().trim(),
      json['word_lt'].toString().trim(),
      json['description_lt'].toString().trim(),
    );
  }

  Map<String, dynamic> toJson() =>
      {
        'id': id,
        'word': word,
        'word_lt': wordLt,
        'description': description,
        'description_lt': descriptionLt
      };

  static Map<String, List<Word>> words = {
    'А': [],
    'Б': [],
    'В': [],
    'Г': [],
    'Д': [],
    'Е': [],
    'Ё': [],
    'Ж': [],
    'З': [],
    'И': [],
    'Й': [],
    'К': [],
    'Л': [],
    'М': [],
    'Н': [],
    'О': [],
    'П': [],
    'Р': [],
    'С': [],
    'Т': [],
    'У': [],
    'Ф': [],
    'Х': [],
    'Ц': [],
    'Ч': [],
    'Ш': [],
    'Э': [],
    'Ю': [],
    'Я': [],
    'Ў': [],
    'Қ': [],
    'Ғ': [],
    'Ҳ': [],
    '-': [],
  };
  static Map<String, List<Word>> wordsLt = {
    'A': [],
    'B': [],
    'D': [],
    'E': [],
    'F': [],
    'G': [],
    'H': [],
    'I': [],
    'J': [],
    'K': [],
    'L': [],
    'M': [],
    'N': [],
    'O': [],
    'P': [],
    'Q': [],
    'R': [],
    'S': [],
    'T': [],
    'U': [],
    'V': [],
    'X': [],
    'Y': [],
    'Z': [],
    'O‘': [],
    'G‘': [],
    'SH': [],
    'CH': [],
    '-': [],
  };
  static List<int> history = [];
  static List<int> favourites = [];
  static Dio dio = Dio();
  static DateTime lastUpdate;
  static int appAlphabet = 0;
  static List resource=[];
  static Map<int, String> alphabets = {0: 'O‘zbek', 1: 'Ўзбек'};
  static Map<String, List<String>> appLabels = {
    'title_home_part_1': ['O‘zbek tilining', 'Ўзбек тилининг'],
    'title_home_part_2': ['izohli lug‘ati', 'изоҳли луғати'],
    'title_home_drawer': ['Bosh sahifa', 'Бош саҳифа'],
    'title_favourites': ['Tanlanganlar', 'Танланганлар'],
    'title_history': ['Tarix', 'Тарих'],
    'title_search': ['Qidirish', 'Қидириш'],
    'first_load': [
      '   Iltimos, internet aloqasi telefoningizda borligiga ishonch hosil qiling, dasturni ilk bor ishga tushirganda so‘zlar bazasini internetdan yuklab olish kerak. Biroz kuting...',
      '   Илтимос, интернет алоқаси телефонингизда борлигига ишонч ҳосил қилинг, дастурни  илк бор ишга туширганда сўзлар базасини интернетдан юклаб олиш керак. Бироз кутинг...',
    ],
    'first_load_error': [
      '   Internet aloqasi mavjud emas. Iltimos, aloqani tiklab dasturni qaytadan ishga tushiring.',
      '   Интернет алоқаси мавжуд эмас. Илтимос, интернетни ёқиб дастурни қайтадан ишга туширинг.',
    ],
    'title_share': ['Ulashish', 'Улашиш'],
  };

  static getWordsFromDatabase(int skip, int getUpTo) async {
    var url;
    if (Word.lastUpdate != null) {
      var convertedDateTime = Word.lastUpdate.toString();
      convertedDateTime.replaceAll('T', ' ');
      url = MyApp.url + '/words/$convertedDateTime/$skip/$getUpTo';
    } else {
      url = MyApp.url + '/words/not/$skip/$getUpTo';
    }
    var response = await dio.get(url);
    List objects = (response.data) as List;
    var newWords = (objects.map((i) => Word.fromJson(i))).toList();
    var batch = MyApp.resource.batch();
    for (var word in newWords) {
      String letter;
      try {
        letter = word.word.substring(0, 1);
      } catch (_) {
        letter = '-';
      }
      if (Word.words[letter] == null) {
        letter = '-';
      }
      if (Word.words[letter].isNotEmpty) {
        for (int i = 0; i < Word.words[letter].length; i++) {
          if (Word.words[letter][i].id == word.id) {
            word.isFavourite = Word.words[letter][i].isFavourite;
            Word.words[letter].remove(Word.words[letter][i]);
            Word.words[letter].add(word);
            break;
          } else {
            Word.words[letter].add(word);
            break;
          }
        }
      } else {
        Word.words[letter].add(word);
      }

      String letterLt;
      try {
        letterLt = word.wordLt.substring(0, 2);
      } catch (_) {}
      if (Word.wordsLt[letterLt] == null) {
        try {
          letterLt = word.wordLt.substring(0, 1);
        } catch (_) {
          letterLt = '-';
        }
      }
      if (Word.wordsLt[letterLt] == null) {
        letterLt = '-';
      }
      if (Word.wordsLt[letterLt].isNotEmpty) {
        for (int i = 0; i < Word.wordsLt[letterLt].length; i++) {
          if (Word.wordsLt[letterLt][i].id == word.id) {
            word.isFavourite = Word.wordsLt[letterLt][i].isFavourite;
            Word.wordsLt[letterLt].remove(Word.wordsLt[letterLt][i]);
            Word.wordsLt[letterLt].add(word);
            break;
          } else {
            Word.wordsLt[letterLt].add(word);
            break;
          }
        }
      } else {
        Word.wordsLt[letterLt].add(word);
      }
      batch.insert('Words', word.toJson(), conflictAlgorithm: ConflictAlgorithm.replace);
    }
    await batch.commit(noResult: true);
    return response;
  }

  static getNewWordsNumber() async {
    var url;
    if (Word.lastUpdate != null) {
      var convertedDateTime = Word.lastUpdate.toString();
      convertedDateTime.replaceAll('T', ' ');
      url = MyApp.url + '/words_number/$convertedDateTime';
    } else {
      url = MyApp.url + '/words_number/not';
    }
    var response = await dio.get(url);
    if (response.data != 0) {
      if (response.data <= 1000) {
        MyApp.requestSize = response.data;
        FirstLoad.times = 1;
      } else if (response.data > 1000 && response.data < 10000) {
        MyApp.requestSize = response.data ~/ 9;
        FirstLoad.times = 10;
      } else if (response.data > 10000 && response.data < 20000) {
        MyApp.requestSize = response.data ~/ 19;
        FirstLoad.times = 20;
      } else if (response.data > 20000 && response.data < 40000) {
        MyApp.requestSize = response.data ~/ 29;
        FirstLoad.times = 30;
      } else {
        MyApp.requestSize = response.data ~/ 49;
        FirstLoad.times = 50;
      }
    }
  }

  static needFullUpdate() async {
    var url = MyApp.url + '/fullUpdate';
    try {
      var response = await dio.get(url);
      if (response.data > MyApp.baseFullUpdate) {
        for (var key in Word.words.keys) {
          Word.words[key].clear();
        }
        Word.history.clear();
        Word.favourites.clear();
        Word.lastUpdate = null;
        await MyApp.wordBase.clear();
        await MyApp.resource.rawDelete('DELETE FROM Words');

        MyApp.baseFullUpdate = response.data;
      }
    } catch (_) {}
  }

  static getRemovedWordsFromDatabase(int skip, int getUpTo) async {
    var url;
    if (Word.lastUpdate != null) {
      var convertedDateTime = Word.lastUpdate.toString();
      convertedDateTime.replaceAll('T', ' ');
      url = MyApp.url + '/removed_words/$convertedDateTime/$skip/$getUpTo';
    } else {
      url = MyApp.url + '/removed_words/not/$skip/$getUpTo';
    }

    var response = await dio.get(url);
    var removedWords =
    ((response.data) as List).map((i) => Word.fromJson(i)).toList();
    for (var word in removedWords) {
      String letter;
      try {
        letter = word.word.substring(0, 1);
      } catch (_) {
        letter = '-';
      }
      String letterLt;
      try {
        letterLt = word.wordLt.substring(0, 2);
      } catch (_) {}
      if (Word.wordsLt[letterLt] == null) {
        try {
          letterLt = word.wordLt.substring(0, 1);
        } catch (_) {
          letterLt = '-';
        }
      }
      for (var base_word in Word.words[letter]) {
        if (base_word.id == word.id) {
          Word.words[letter].remove(base_word);
          break;
        }
      }

      for (var base_word in Word.wordsLt[letterLt]) {
        if (base_word.id == word.id) {
          Word.wordsLt[letterLt].remove(base_word);
          break;
        }
      }

      MyApp.resource.delete('Words', where: 'id = ${word.id}');

      for (var base_word_id in Word.favourites) {
        if (base_word_id == word.id) {
          Word.favourites.remove(base_word_id);
          await MyApp.wordBase.put('favourites', Word.favourites);
          break;
        }
      }
      for (var base_word_id in Word.history) {
        if (base_word_id == word.id) {
          Word.history.remove(base_word_id);
          await MyApp.wordBase.put('history', Word.history);
          break;
        }
      }
    }
    return response;
  }

  static changeFavouriteParam(Word word) async {
    word.isFavourite = !word.isFavourite;
    if (!Word.favourites.contains(word.id)) {
      Word.favourites.add(word.id);
    } else {
      Word.favourites.remove(word.id);
    }
    await MyApp.wordBase.put('favourites', Word.favourites);
  }

  static addToHistory(Word word) async {
    if (Word.history.contains(word.id)) {
      Word.history.remove(word.id);
      Word.history.add(word.id);
    } else {
      Word.history.add(word.id);
    }
    await MyApp.wordBase.put('history', Word.history);
  }
}
