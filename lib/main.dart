import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:ilugat/first_load.dart';
import 'package:ilugat/word.dart';
import 'package:sqflite/sqflite.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
  await Hive.initFlutter();
  await Hive.openBox(MyApp.wordLocalBase);
  MyApp.resource = await openDatabase('resource.db', version: 1,
      onCreate: (Database db, int version) async {
        // When creating the db, create the table
        await db.execute(
            'CREATE TABLE Words (id INTEGER PRIMARY KEY, word TEXT, description TEXT, word_lt TEXT, description_lt TEXT)');
      });

  if (MyApp.wordBase.get('last_update') != null) {
    Word.lastUpdate = MyApp.wordBase.get('last_update');
  } else {
    Word.lastUpdate = null;
  }
  if (MyApp.wordBase.get('appAlphabet') != null) {
    Word.appAlphabet = await MyApp.wordBase.get('appAlphabet');
  }

  if (MyApp.wordBase.get('favourites') != null) {
    Word.favourites = await MyApp.wordBase.get('favourites');
  } else {
    Word.favourites = [];
  }
  Word.resource = await MyApp.resource.query('Words');
  if (Word.resource.isNotEmpty) {
    for (var object in Word.resource) {
      Word word = Word.fromJson(object);
      if(Word.favourites.contains(word.id)){
        word.isFavourite=true;
      }
      String cyrillLetter = word.word.substring(0, 1);
      String latinLetter;

      try{
        latinLetter = word.wordLt.substring(0, 2);
      }catch(_){}
      if(Word.wordsLt[latinLetter]==null){
        try{
          latinLetter = word.wordLt.substring(0, 1);
        }catch(_){
          latinLetter='-';
        }
      }
      if(Word.wordsLt[latinLetter]==null){
        latinLetter='-';
      }

      if(Word.words[cyrillLetter]==null){
        cyrillLetter='-';
      }
      Word.words[cyrillLetter].add(word);
      Word.wordsLt[latinLetter].add(word);
    }
  }

  if (MyApp.wordBase.get('history') != null) {
    Word.history = await MyApp.wordBase.get('history');
  } else {
    Word.history = [];
  }
  if (MyApp.wordBase.get('fullUpdate') != null) {
    MyApp.baseFullUpdate = await MyApp.wordBase.get('fullUpdate');
  } else {
    MyApp.baseFullUpdate = 0;
  }
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  static String url = 'http://api.ilugat.uz';
  static String wordLocalBase = 'Word_Local_Base';
  static Database resource;
  static int baseFullUpdate;
  static Box wordBase = Hive.box(MyApp.wordLocalBase);
  static int requestSize = 0;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'ilugat',
      theme: ThemeData(platform: TargetPlatform.iOS),
      home: FirstLoad(),
    );
  }
}
