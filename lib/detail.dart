import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:ilugat/word.dart';

class Detail extends StatefulWidget {
  final Word word;

  const Detail({Key key, this.word}) : super(key: key);

  @override
  _DetailState createState() => _DetailState();
}


class _DetailState extends State<Detail> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
          horizontal: MediaQuery.of(context).size.width * 0.02),
      child: Column(
        children: [
          Container(
            width: MediaQuery.of(context).size.width * 0.76,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(32.0),
              color: Colors.white,
            ),
            child: Padding(
              padding: EdgeInsets.symmetric(
                horizontal: MediaQuery.of(context).size.width * 0.05,
                vertical: MediaQuery.of(context).size.height * 0.02,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10.0),
                    child: Text(
                      (Word.appAlphabet==1)?widget.word.word:widget.word.wordLt,
                      style: GoogleFonts.ptSans(
                        color: Color(0xFF35c79c),
                        shadows: [
                          BoxShadow(
                            blurRadius: 2,
                            color: Color(0xFFa4fff7),
                          ),
                        ],
                        fontSize: 18,
                        fontWeight: FontWeight.w600,
                      ),
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                  Container(
                    height: 0.5,
                    width: MediaQuery.of(context).size.width * 0.68,
                    color: Colors.grey.withOpacity(0.5),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10.0),
                    child: SelectableText(
                      (Word.appAlphabet==1)?'\t' + widget.word.description:'\t' + widget.word.descriptionLt,
                      style: GoogleFonts.ptSans(
                        color: Color(0xFF030a4e),
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                      ),
                      cursorColor: Color(0xFF030a4e),
                      showCursor: true,
                      enableInteractiveSelection: true,
                      toolbarOptions: ToolbarOptions(
                        copy: true,
                        selectAll: true,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
